--------------------
dces-dtrhf-ser1ch-v1
--------------------

Here is what the name stands for:
Data Center Environmental Sensor sensing Dust, Temperature, Relative Humidity and Fan speed sending results on SERial 1 CHannel - version 1.

This is a long name, but it precisely defines what is implemented in the project.


Compiling and flashing the firmware
-----------------------------------

Some prerequites, to compile this firmware, you need to install the ``arduino-core`` package, the full IDE is not needed, because it is simple an faster to build from the commandline:

::

 $ yum instal arduino-core

Before going any further you need to adapt the configuration file to your setup: edit ``dces-dtrhf-ser1ch-v1.conf`` in ``dces-dtrhf-ser1ch-v1/arduino/``.

Then to build and flash the arduino board, simply run ``build.sh`` from command line:

::

 $ cd dces-dtrhf-ser1ch-v1/arduino
 $ ./build.sh

It is only going to build ``dces-dtrhf-ser1ch-v1/arduino/final/final.ino`` using ``dces-dtrhf-ser1ch-v1/arduino/final/Makefile``.

To clean the project, just ``make clean`` from ``dces-dtrhf-ser1ch-v1/arduino/final`` directory.

If ``UPLOAD=true`` in ``dces-dtrhf-ser1ch-v1.conf``, the firmware is going to be flashed on the arduino board.


Configuring the firmware
------------------------

Firmware configuration is done from the arduino serial port using ``SerialCommand`` library.

When the firmware starts it displays measured data by default:

::

 $ picocom /dev/ttyACM0  
 port is        : /dev/ttyACM0
 flowcontrol    : none
 baudrate is    : 9600
 parity is      : none
 databits are   : 8
 escape is      : C-a
 local echo is  : no
 noinit is      : no
 noreset is     : no
 nolock is      : no
 send_cmd is    : sz -vv
 receive_cmd is : rz -vv
 imap is        : 
 omap is        : 
 emap is        : crcrlf,delbs,
 
 Terminal ready
     8471,    8384,     446, 26.69, 31.37, 196, 0,  960
     8419,    8448,     163, 26.69, 31.44, 196, 0,  960
     ...


