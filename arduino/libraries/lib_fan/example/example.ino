/******************************************************************************
 *
 * This file is part of the DCES project.
 * See http://www.ohwr.org/projects/dces-dtrhf-ser1ch-v1
 *
 * Copyright (C) 2016 CERN
 * All rights not expressly granted are reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author DCES Dev team, dces-dev@cern.ch
 *****************************************************************************/


// Fan control and RPM measurement example:
// You need to include lib_fan.h
#include <lib_fan.h>

unsigned int duty_cycle = 0;
unsigned int speed = 0;
bool bSpeed = false;

void setup(){
  Serial.begin(9600);
  initFan();					// Don't forget to call initFan() to set PWM/Tachometer pins
}

void loop(){
  setRPM(duty_cycle);				// Call setRPM(value) to set the speed of the fan (value in percent)
  delay(2000);
  speed = getRPM();				// Call getRPM will return the instant RPM of the fan (or 0 if timeout)
  Serial.println(speed);
  if(duty_cycle<100 && bSpeed==false){
    duty_cycle += 5;
  }
  else if(duty_cycle==100 && bSpeed==false){
    bSpeed = true;
  }
  else if(duty_cycle>0 && bSpeed==true){
    duty_cycle -= 5;
  }
  else if(duty_cycle==0 && bSpeed==true){
    bSpeed = false;
  }
}
