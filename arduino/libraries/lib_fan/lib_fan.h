/******************************************************************************
 *
 * This file is part of the DCES project.
 * See http://www.ohwr.org/projects/dces-dtrhf-ser1ch-v1
 *
 * Copyright (C) 2016 CERN
 * All rights not expressly granted are reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author DCES Dev team, dces-dev@cern.ch
 *****************************************************************************/


#include <Arduino.h>

#define FAN_SPEED 25           		   // Duty-cycle of PWM in %
#define RPM_TIMEOUT 200      		   // RPM Measure Timeout in milliseconds
#define RPMMIN 700
#define RPMMAX 10000

void initFan(void);			   // Used to initialize PWM and Tachometer
unsigned int getRPM(void);		   // Used to get instant RPMs (returns the value)
void setRPM(unsigned int pct);		   // Used to set the fan speed (in percent)
void setRPMTimeout(unsigned int timeout);  // Used to set an other RPM measure timeout
