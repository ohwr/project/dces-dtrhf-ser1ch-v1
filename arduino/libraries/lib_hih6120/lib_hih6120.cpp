/******************************************************************************
 *
 * This file is part of the DCES project.
 * See http://www.ohwr.org/projects/dces-dtrhf-ser1ch-v1
 *
 * Copyright (C) 2016 CERN
 * All rights not expressly granted are reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author DCES Dev team, dces-dev@cern.ch
 *****************************************************************************/


#include <lib_hih6120.h>

uint8_t high_Temp, low_Temp, high_Humi, low_Humi;  // 4 bytes for I2C readings
uint16_t tmp_Temp, tmp_Humi;		       // 2 bytes temporary variables

// INITIALIZE I2C COMMUNICATIONS FOR HIH-6120
void initI2C(void){
  I2c.begin();                     // Join i2c bus (address optional for master)
  I2c.timeOut(200);                // Set an I2C timeout of 100 ms
  I2c.setSpeed(1);                 // Set high I2C speed (400 kHz)
  I2c.pullup(0);                   // Disable internal pullup resistors 
}

// MAKE HIH-6120 UPDATE TEMPERATURE/HUMIDITY
void updateTH(void){
  I2c.write(0x27, 0x00, 0);        // Make HIH-6120 updates its values.
}

// GET TEMPERATURE
float getTemp(void){
  float temp = (((float)tmp_Temp/16382)*165)-40;      // Calculating the value of temperature in celsius degrees
  return temp;
}

// GET HUMIDITY
float getHumi(void){
  float humi = ((float)tmp_Humi/16382)*100;           // Calculating the value of humidity in percent
  return humi;
}

// START TIMER
void startTimer(unsigned long* pTime){
  *pTime = micros();
}

// END TIMER
unsigned long endTimer(unsigned long* pTime){
  unsigned long end = micros();
  unsigned long delta=0;
  delta = end - *pTime;                  // calculate the time spent while acquiring data
  return delta;
}


// GET TEMPERATURE/HUMIDITY
int getTH(unsigned long* pTime){ 
  int HIH_state=1;
  startTimer(pTime);		      // Fill pTime with the time in us
  I2c.read(0x27, 4);                  // Request 4 bytes from HIH-6120 @ 0x27
  if(I2c.available()==4){             // Get the 4 bytes containing:
    high_Humi =  I2c.receive();       // Humidity (on 14 bits) for the 2 first bytes
    low_Humi  =  I2c.receive();
    high_Temp =  I2c.receive();       // Temperature (on 14 bits) for the 2 last bytes
    low_Temp  =  I2c.receive();
  }
  *pTime = endTimer(pTime);           // Returns the time difference between pTime value and now
  HIH_state = (int)((high_Humi & 0xC0) >> 6);        // Get the 2 status bits (00 -> Ok, 01 -> Stale values)
  // Compute the values (humidity/temperature)
  tmp_Temp = ((((uint16_t) high_Temp) << 8) | low_Temp) >> 2;      // Assembly of 2 bytes of the temperature
  tmp_Humi = (((uint16_t) (high_Humi & 0x3f)) << 8) | low_Humi;    // Assembly of 2 bytes of the humidity
  return HIH_state;
}

