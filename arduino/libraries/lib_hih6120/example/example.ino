/******************************************************************************
 *
 * This file is part of the DCES project.
 * See http://www.ohwr.org/projects/dces-dtrhf-ser1ch-v1
 *
 * Copyright (C) 2016 CERN
 * All rights not expressly granted are reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author DCES Dev team, dces-dev@cern.ch
 *****************************************************************************/


// Temperature/Humidity measurement with HIH-6120 example:
// You need to include lib_hih6120.h
#include <lib_hih6120.h>

float fT;
float fH;
unsigned long measTime=0;
int hih_state;

void setup(){
  Serial.begin(9600);
  initI2C();				// Don't forget to call initI2C() to be able to communicate with HIH-6120
}

void loop(){
  updateTH();				// Call updateTH() when you want the HIH-6120 to perform measures
  delay(50);
  hih_state = getTH(&measTime);		// Call getTH(unsigned int* pTime) to download HIH-6120 values in a buffer
					// getTH() takes an unsigned int address (pointer) that will filled with the
					// duration of the data transfert (in us) and returns the validity of the measure:
					// 0: This is an unread value, 1: The value has already been read
					// To be sure that the value is unread, let at least 40 ms between updateTH()
					// and getTH()
  
  fT = getTemp();			// Call getTemp() will return the value of the temperature (in *C) from the buffer
  
  fH = getHumi();			// Call getHumi() will return the value of the humidity (in %) from the buffer
  Serial.print("Temperature: ");
  Serial.print(fT);
  Serial.println("*C");
  Serial.print("Humidity: ");
  Serial.print(fH);
  Serial.println("%");
  Serial.print("Measure duration: ");
  Serial.print(measTime);
  Serial.println("us");
  Serial.print("Measure validity: ");
  Serial.println(hih_state);
  Serial.println("");
  delay(2000);
}
