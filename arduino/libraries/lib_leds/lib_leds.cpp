/******************************************************************************
 *
 * This file is part of the DCES project.
 * See http://www.ohwr.org/projects/dces-dtrhf-ser1ch-v1
 *
 * Copyright (C) 2016 CERN
 * All rights not expressly granted are reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author DCES Dev team, dces-dev@cern.ch
 *****************************************************************************/


#include <lib_leds.h>

void initLEDs(void){
  DDRK |= (1 << DDK7);
  DDRK |= (1 << DDK6);
  PORTK &= ~(1 << DDK7);
  PORTK &= ~(1 << DDK6);
}

void controlLED15(bool on){
  if(on){
    PORTK |= (1 << DDK7);
  }
  else{
    PORTK &= ~(1 << DDK7);
  }
}

void controlLED14(bool on){
  if(on){
    PORTK |= (1 << DDK6);
  }
  else{
    PORTK &= ~(1 << DDK6);
  }
}

void toggleLED15(bool tog){
  bool b=((PINK & 0b10000000)>>7);
  if(b && tog){
    PORTK &= ~(1 << DDK7);
  }
  else if(~b && tog){
    PORTK |= (1 << DDK7);
  }
  if(!tog){
    PORTK &= ~(1 << DDK7);
  } 
}

void toggleLED14(bool tog){
  bool b=((PINK & 0b01000000)>>6);
  if(b && tog){
    PORTK &= ~(1 << DDK6);
  }
  else if(~b && tog){
    PORTK |= (1 << DDK6);
  }
  if(!tog){
    PORTK &= ~(1 << DDK6);
  } 
}

