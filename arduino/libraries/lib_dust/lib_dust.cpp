/******************************************************************************
 *
 * This file is part of the DCES project.
 * See http://www.ohwr.org/projects/dces-dtrhf-ser1ch-v1
 *
 * Copyright (C) 2016 CERN
 * All rights not expressly granted are reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author DCES Dev team, dces-dev@cern.ch
 *****************************************************************************/


#include <lib_dust.h>

// GLOBALS
unsigned int dustPin=0;
unsigned int ledPower=2;
unsigned int delayTime=280;
unsigned int delayTime2=40;
float offTime=9680; // so that delayTime + delayTime2 + offTime = 10ms
unsigned int skip_measurements=50; // skip the first 50 mesurements of a series

unsigned int dustVal=0;
float voltage = 0;
float dustdensity = 0;
float ppmpercf = 0;

unsigned int dustValues[NB_MEAS]; // array storing all the measured values (10 bit)
// since we'll be doing an average over several values, we can increase the precision of each measurement to more bits
// with a simple bit shift without fearing an overflow
unsigned long sum; // to store the sum of measured dust values (4 bytes): NB_MEAS < 4194303 to avoid overflow => OK

// INITIALIZE DUST PIN
void initDust(void){
  pinMode(ledPower,OUTPUT);
}

// MEASURE DUST
float measureDust(float &avgRetDust, float &medRetDust, float &stdevRetDust){
  unsigned int i,j; // loop indexes
  unsigned int temp; // temporary dust value
  float medianDust, varDust, stdevDust, avgDust; // some statistical values

  // skip initial measurements see dust_calib
  for (i=0; i<skip_measurements; i++) {
    digitalWrite(ledPower,LOW);     // power on the LED
    delayMicroseconds(delayTime);
    delayMicroseconds(delayTime2);
    digitalWrite(ledPower,HIGH);    // turn the LED off
    delayMicroseconds(offTime);
  }

  // Let s do the real measurements
  for (i=0; i<NB_MEAS; i++) {
    digitalWrite(ledPower,LOW);     // power on the LED
    delayMicroseconds(delayTime);
    dustValues[i]=analogRead(dustPin) << 6;    // read the dust value and shift the 10bit ADC value by 6 bits
    delayMicroseconds(delayTime2);
    digitalWrite(ledPower,HIGH);    // turn the LED off
    delayMicroseconds(offTime);
  }
#ifdef DEBUG
  // display values in measurement order first
  // because later the array is going to be sorted to find the median
  Serial.println("all values:");
  for (i=0; i<NB_MEAS; i++) {
    Serial.print(i);
    Serial.print(" ");
    Serial.println(dustValues[i]);
  }
#endif
  // compute median values
  for (i=0; i<NB_MEAS-1; i++) { // careful: stopping 1 index before the end
    for(j=i+1; j<NB_MEAS; j++) {
      if(dustValues[j] < dustValues[i]) {
        // swap elements
        temp = dustValues[i];
        dustValues[i] = dustValues[j];
        dustValues[j] = temp;
      }
    }
  }
  // compute avg
  sum = 0;
  for (i=0; i<NB_MEAS; i++) {
    sum += dustValues[i];
  }
  avgDust = sum/NB_MEAS;
    // compute variance
    for (i=0; i<NB_MEAS; i++) {
      varDust = (dustValues[i]-avgDust)*(dustValues[i]-avgDust);
    }
  varDust = varDust/NB_MEAS;
  stdevDust = sqrt(varDust);
  // now dustValues is sorted
  if(NB_MEAS%2==0) {
    // if there is an even number of elements, return mean of the two elements in the middle
    medianDust = (dustValues[NB_MEAS/2] + dustValues[NB_MEAS/2 - 1]) / 2.0;
  } else {
    // else return the element in the middle
    medianDust = dustValues[NB_MEAS/2];
  }
#ifdef DEBUG
  Serial.print("medianDust: ");
  Serial.println(medianDust, 3);
  Serial.print("stdevDust: ");
  Serial.println(stdevDust,3);
  Serial.print("avgDust: ");
  Serial.println(avgDust,3);
#endif
  // copy those in the results with no scaling
  avgRetDust = avgDust;
  medRetDust = medianDust;
  stdevRetDust = stdevDust;

  // default scaling for compatibility reasons... 
  voltage = avgDust*0.0049;
  dustdensity = 0.17*voltage-0.1;
  ppmpercf = (voltage-0.0256)*120000;
  if (ppmpercf < 0)
    ppmpercf = 0;
  if (dustdensity < 0 )
    dustdensity = 0;
  if (dustdensity > 0.5)
    dustdensity = 0.5;

  return ppmpercf;
}
