#!/bin/bash


 ##############################################################################
 #
 # This file is part of the DCES project.
 # See http://www.ohwr.org/projects/dces-dtrhf-ser1ch-v1
 #
 # Copyright (C) 2016 CERN
 # All rights not expressly granted are reserved.
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 3
 # of the License, or (at your option) any later version.
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 # You should have received a copy of the GNU General Public License
 # along with this program; if not, write to the Free Software
 # Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 #
 # @author DCES Dev team, dces-dev@cern.ch
 #############################################################################


. ./dces-dtrhf-ser1ch-v1.conf 2>/dev/null

cd final

if [[ $UPLOAD = "true" ]]
then
    echo "Compiling and upgrading arduino firmware"
    make BOARD=$BOARD SERIALDEV=$DEVICE upload
else
    echo "Compiling only"
    make BOARD=$BOARD SERIALDEV=$DEVICE
fi
