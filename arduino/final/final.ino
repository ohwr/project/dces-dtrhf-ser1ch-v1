/******************************************************************************
 *
 * This file is part of the DCES project.
 * See http://www.ohwr.org/projects/dces-dtrhf-ser1ch-v1
 *
 * Copyright (C) 2016 CERN
 * All rights not expressly granted are reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author DCES Dev team, dces-dev@cern.ch
 *****************************************************************************/


#include <stdlib.h>
#include <EEPROM.h>
#include <string.h>
#include <lib_hih6120.h>
#include <lib_fan.h>
#include <lib_dust.h>
#include <lib_leds.h>
#include <SoftwareSerial.h>
#include <SerialCommand.h>

#define WAIT_TIME 2500   // Waiting time before restarting loop() in millis
#define NMEAS 1          // Number of dust measures between 2 humidity/temperature measures
#define FSR 0b11111111   // Full Scale Range

// ADDRESSES of config
#define DCYADDR 0
#define RPMTOADDR 2
#define NMEASADDR 4
#define WTIMEADDR 6
#define RPMMINADDR 8
#define RPMMAXADDR 10

// LENGTH of the different FIELDS:
#define TLENGTH 6      // Temperature
#define HLENGTH 6      // Humidity
#define DLENGTH 8      // Dust Level
#define TIMELENGTH 4   // Time
#define SLENGTH 2      // State
#define RPMLENGTH 5    // RPM

// PRECISION of the different FIELDS (change length if you change precision!):
#define TPREC 2	       // Temperature
#define HPREC 2        // Humidity
#define VPREC 4	       // Voltage
#define DPREC 2        // Dust (unused)

SerialCommand SCmd;

//FUNCTIONS:
void StrAppend(char* dest, char* source, int length, boolean strEnd);  // Put string source at the end of string dest.
int readCmd(void);
void command(void);
void EEPROM_write_int(int addr, int val);
int EEPROM_read_int(int addr);			       
void SET_PWM(void);
void SET_MES(void);
void SET_RTO(void);
void SET_WTT(void);
void SET_RMN(void);
void SET_RMX(void);
void SET_DEF(void);
void GET_PWM(void);
void GET_MES(void);
void GET_RTO(void);
void GET_WTT(void);
void GET_RMN(void);
void GET_RMX(void);
void GET_ALL(void);
void GET_RPM(void);
void EEP_BRN(void);
void EEP_RST(void);
void EEP_PRT(void);
void RUN(void);
void STOP(void);
void HELP(void);
void unrecognized(void);

// COMMON:
unsigned int index=0;	     // Index of StrAppend() function to locate the position in the dest. string.
char s[16];
char dataStr[TLENGTH+HLENGTH+DLENGTH+DLENGTH+DLENGTH+TIMELENGTH+SLENGTH+RPMLENGTH+10];
unsigned int nMeasConf = NMEAS;
unsigned int nMeasure = nMeasConf-1;	     // Used to update humidity/temperature every NMEAS dust measures
unsigned int waitTime = WAIT_TIME;
bool bRun = true;
bool bErr = false;

// DUST INITIALIZATION PART:
float avgDust, medDust, stdevDust;

// HUMIDITY/TEMPERATURE INITIALIZATION PART:
float fTemp, fHumi;          // Final values in Celsius degrees (temp) and in percent (humi)
float fTempPrev, fHumiPrev;  // Save of the previous values
unsigned long deltaTime;     // Duration of the humidity/temperature measure in micros
unsigned int hih_state;      // HIH sensor state: 0 -> Ok, 1 -> Stale values

// FAN INITIALIZATION PART
unsigned int rpm;
unsigned int rpmTimeout = RPM_TIMEOUT;
unsigned int fanSpeed = FAN_SPEED;
unsigned int rpmMax = RPMMIN;
unsigned int rpmMin = RPMMAX;


//SETUP:
void setup(){
  Serial.begin(9600);
  initDust();
  initI2C();
  initFan();
  initLEDs();
  controlLED14(true);
  controlLED15(true);
  rpmMin = EEPROM_read_int(RPMMINADDR);
  rpmMax = EEPROM_read_int(RPMMAXADDR);			       
  SCmd.addCommand("SET_PWM",SET_PWM);
  SCmd.addCommand("SET_MES",SET_MES);
  SCmd.addCommand("SET_RTO",SET_RTO);
  SCmd.addCommand("SET_WTT",SET_WTT);
  SCmd.addCommand("SET_RMN",SET_RMN);
  SCmd.addCommand("SET_RMX",SET_RMX);
  SCmd.addCommand("SET_DEF",SET_DEF);
  SCmd.addCommand("GET_PWM",GET_PWM);
  SCmd.addCommand("GET_MES",GET_MES);
  SCmd.addCommand("GET_RTO",GET_RTO);
  SCmd.addCommand("GET_WTT",GET_WTT);
  SCmd.addCommand("GET_RMN",GET_RMN);
  SCmd.addCommand("GET_RMX",GET_RMX);
  SCmd.addCommand("GET_ALL",GET_ALL);
  SCmd.addCommand("GET_RPM",GET_RPM);
  SCmd.addCommand("EEP_BRN",EEP_BRN);
  SCmd.addCommand("EEP_RST",EEP_RST);
  SCmd.addCommand("EEP_PRT",EEP_PRT);
  SCmd.addCommand("RUN",RUN);
  SCmd.addCommand("STOP",STOP);
  SCmd.addCommand("HELP",HELP);
  SCmd.addDefaultHandler(unrecognized);
  EEP_RST();
  delay(1000);
  controlLED14(false);
  controlLED15(false);
}

//LOOP:
void loop(){
  SCmd.readSerial();
  //controlLED14(bRun);
  if(bRun==false){
    controlLED14(false);
    delay(400);
    controlLED14(true);
    delay(400);
  }
  if(bRun==true){
    // Make HIH-6120 updates its values
    updateTH();

    controlLED14(true);

    // Dust measurement
    measureDust(avgDust, medDust, stdevDust);
    nMeasure++;
   
    // Temperature/humidity measurement
    if(nMeasure == nMeasConf) { 
      fTempPrev = fTemp;
      fHumiPrev = fHumi; 
      hih_state = getTH(&deltaTime);
      fTemp = getTemp();
      fHumi = getHumi(); 
      nMeasure = 0; 
    }
    
    // RPM measurement
    rpm = getRPM();
      
    // Test values to check if they are consistent 
    bErr = false;
 
    if(rpm==0 || rpm>rpmMax || rpm<rpmMin){
      rpm = 0;
      bErr = true;
    }
     
    if(fTemp<0 || fTemp>90){       
      fTemp=fTempPrev;
      bErr = true;
    }
    
    if(fHumi<0 || fHumi>100){ 
      fHumi=fHumiPrev; 
      bErr = true;
    }
    
    if(hih_state==1){
      bErr = true;
    }

    controlLED15(bErr);

    // CONCATENING & SENDING (to the Raspberry Pi):  
    dataStr[0]='\0';
    s[0]='\0';
    dtostrf(avgDust, DLENGTH, 0, s);
    StrAppend(dataStr, s, DLENGTH, false);

    dtostrf(medDust, DLENGTH, 0, s);
    StrAppend(dataStr, s, DLENGTH, false);

    dtostrf(stdevDust, DLENGTH, 0, s);
    StrAppend(dataStr, s, DLENGTH, false);

    dtostrf(fTemp, TLENGTH, TPREC, s);
    StrAppend(dataStr, s, TLENGTH, false);

    dtostrf(fHumi, HLENGTH, HPREC, s);
    StrAppend(dataStr, s, HLENGTH, false);

    dtostrf(deltaTime, TIMELENGTH, 0, s);
    StrAppend(dataStr, s, TIMELENGTH, false);

    dtostrf(hih_state, SLENGTH, 0, s);
    StrAppend(dataStr, s, SLENGTH, false);

    dtostrf(rpm, RPMLENGTH, 0, s);
    StrAppend(dataStr, s, RPMLENGTH, true);

    Serial.println(dataStr);

    // Wait 2.5 seconds before starting the next series of measurements
    delay(waitTime);
  }
}
  



// CONCATENATION OF THE RESULT
void StrAppend(char* strDest, char* strSource, int iLen, boolean bEnd){
  int i;
  for(i=0;i<=iLen && strSource[i]!='\0';i++){
    strDest[index+i] = strSource[i];
  }
  if(bEnd == true){
    strDest[index+i]='\0';
    index = 0;
  }
  else{
    strDest[index+i] =',';
    strDest[index+i+1] = '\0';
    index += iLen+1;
  }
}


// READ AN INT VALUE TYPED BY THE USER (value from 0 to 9999)
int readCmd(char* com){
  int s;
  int m;
  int c;
  int d;
  int u;
  int r;
  if(com[0]==0){
    u = -1;
    d = 0;
    c = 0;
    m = 0;
    s = 0;
  }
  else if(com[1]==0){
    u = com[0]-48;
    d = 0;
    c = 0;
    m = 0;
    s = 0;
  }
  else if(com[2]==0){
    u = (com[1]-48);
    d = (com[0]-48);
    c = 0;
    m = 0;
    s = 0;
  }
  else if(com[3]==0){
    u = (com[2]-48);
    d = (com[1]-48);
    c = (com[0]-48);
    m = 0;
    s = 0;
  }
  else if(com[4]==0){
    u = (com[3]-48);
    d = (com[2]-48);
    c = (com[1]-48);
    m = (com[0]-48);
    s = 0;
  }
  else if(com[4]!=0){
    u = (com[4]-48);
    d = (com[3]-48);
    c = (com[2]-48);
    m = (com[1]-48);
    s = (com[0]-48);
  } 
  r = s*10000 + m*1000 + c*100 + d*10 + u;
  return r;
}


void SET_PWM(void){
  char* arg;
  int tmp;
  arg = SCmd.next();
  tmp = readCmd(arg);
  if(tmp>=0 && tmp<=100){
    fanSpeed = tmp;
    setRPM(fanSpeed);
    Serial.print("OK:PWM=");
    Serial.println(fanSpeed);
  }
  else{
    Serial.println("INVALID VALUE");
  }
}

void SET_MES(void){
  char* arg;
  int tmp;
  arg = SCmd.next(); 
  tmp = readCmd(arg);
  if(tmp>=1 && tmp<=100){
    nMeasConf = tmp;
    nMeasure = nMeasConf - 1;
    Serial.print("OK:MES=");
    Serial.println(nMeasConf);
  }
  else{
    Serial.println("INVALID VALUE");
  }
}  

void SET_RTO(void){
  char* arg;
  int tmp;
  arg = SCmd.next();
  tmp = readCmd(arg);
  if(tmp>=1 && tmp<=1000){
    rpmTimeout = tmp;
    setRPMTimeout(rpmTimeout);
    Serial.print("OK:RTO=");
    Serial.println(rpmTimeout);
  }
  else{
    Serial.println("INVALID VALUE");
  }
}

void SET_WTT(void){
  char* arg;
  int tmp;
  arg = SCmd.next();
  tmp = readCmd(arg);
  if(tmp>=100 && tmp<=20000){
    waitTime = tmp;
    Serial.print("OK:WTT=");
    Serial.println(waitTime);
  }
  else{
    Serial.println("INVALID VALUE");
  }
}

void SET_RMN(void){
  char* arg;
  int tmp;
  arg = SCmd.next();
  tmp = readCmd(arg);
  rpmMin = tmp;
  Serial.print("OK:RMN=");
  Serial.println(rpmMin);
}

void SET_RMX(void){
  char* arg;
  int tmp;
  arg = SCmd.next();
  tmp = readCmd(arg);
  rpmMax = tmp;
  Serial.print("OK:RMX=");
  Serial.println(rpmMax);
}

void SET_DEF(void){
  fanSpeed = FAN_SPEED;
  setRPM(fanSpeed);
  rpmTimeout = RPM_TIMEOUT;
  setRPMTimeout(rpmTimeout);
  nMeasConf = NMEAS;
  nMeasure = NMEAS - 1;
  waitTime = WAIT_TIME;
  rpmMin = RPMMIN; 
  rpmMax = RPMMAX;
  Serial.println("OK:DEFAULT CONFIG RESTORED");
}

void GET_PWM(void){
  Serial.print("PWM=");
  Serial.println(fanSpeed);
}

void GET_MES(void){
  Serial.print("MES=");
  Serial.println(nMeasConf);
}

void GET_RTO(void){
  Serial.print("RTO=");
  Serial.println(rpmTimeout);
}

void GET_WTT(void){
  Serial.print("WTT=");
  Serial.println(waitTime);
}

void GET_RMN(void){
  Serial.print("RMN=");
  Serial.println(rpmMin);
}

void GET_RMX(void){
  Serial.print("RMX=");
  Serial.println(rpmMax);
}

void GET_ALL(void){
  Serial.print("PWM=");
  Serial.println(fanSpeed);
  Serial.print("MES=");
  Serial.println(nMeasConf);
  Serial.print("RTO=");
  Serial.println(rpmTimeout);
  Serial.print("WTT=");
  Serial.println(waitTime);
  Serial.print("RMN=");
  Serial.println(rpmMin);
  Serial.print("RMX=");
  Serial.println(rpmMax);
}

void GET_RPM(void){
  Serial.print("RPM=");
  Serial.println(getRPM());
}

void EEP_BRN(void){
  EEPROM_write_int(DCYADDR, fanSpeed);
  EEPROM_write_int(RPMTOADDR, rpmTimeout);
  EEPROM_write_int(NMEASADDR, nMeasConf);
  EEPROM_write_int(WTIMEADDR, waitTime);
  EEPROM_write_int(RPMMINADDR, rpmMin);
  EEPROM_write_int(RPMMAXADDR, rpmMax);
  Serial.println("CONFIG BURNED IN EEPROM");
}

void EEP_RST(void){
  fanSpeed = EEPROM_read_int(DCYADDR);
  setRPM(fanSpeed);
  rpmTimeout = EEPROM_read_int(RPMTOADDR);
  setRPMTimeout(rpmTimeout);
  nMeasConf = EEPROM_read_int(NMEASADDR);
  nMeasure = nMeasConf - 1;
  waitTime = EEPROM_read_int(WTIMEADDR);
  rpmMin = EEPROM_read_int(RPMMINADDR);
  rpmMax = EEPROM_read_int(RPMMAXADDR); 
  Serial.println("OK:CONFIG RESTORED FROM EEPROM");
}

void EEP_PRT(void){
  Serial.println("EEPROM CONFIG:");
  Serial.print("PWM=");
  Serial.println(EEPROM_read_int(DCYADDR));
  Serial.print("MES=");
  Serial.println(EEPROM_read_int(NMEASADDR));
  Serial.print("RTO=");
  Serial.println(EEPROM_read_int(RPMTOADDR));
  Serial.print("WTT=");
  Serial.println(EEPROM_read_int(WTIMEADDR));
  Serial.print("RMN=");
  Serial.println(EEPROM_read_int(RPMMINADDR));
  Serial.print("RMX=");
  Serial.println(EEPROM_read_int(RPMMAXADDR));
}

void RUN(void){
  bRun = true;
  Serial.println("OK:RUN");
}

void STOP(void){
  bRun = false;
  Serial.println("OK:STOP");
}

void HELP(void){
  Serial.println("List of all commands. Please refer to commands.rst");
  Serial.println("GET_ or SET_"); 
  Serial.println("............PWM");
  Serial.println("............MES");
  Serial.println("............RTO");
  Serial.println("............WTT");
  Serial.println("............RMN/RMX");
  Serial.println("............RPM");
  Serial.println("GET_ALL");
  Serial.println("SET_DEF");
  Serial.println("EEP_");
  Serial.println("....BRN");
  Serial.println("....RST");
  Serial.println("....PRT");
  Serial.println("RUN");
  Serial.println("STOP");
}

void unrecognized(){
  Serial.println("UNKNOWN COMMAND, TYPE HELP TO GET COMMANDS");
}


// WRITES AN INTEGER ON 16 BITS (val) AT SPECIFIED ADDRESS (addr) in EEPROM MEMORY
// Caution: integers are written on 2 memory words (16 bits) which mean you have to space out
// addresses of 2. ie: addr1=0, addr2=2, addr3=4...
void EEPROM_write_int(int addr, int val){
  uint8_t low=0;
  uint8_t high=0;
  
  low = val & FSR;
  high = (val >> 8) & FSR;
  
  EEPROM.write(addr,high);
  EEPROM.write(addr+1,low);
}

// READS AN INTEGER ON 16 BITS AT THE SPECIFIED ADDRESS (addr) IN EEPROM MEMORY
int EEPROM_read_int(int addr){
  uint8_t low=0;
  uint8_t high=0;
  int val=0;
  
  high = EEPROM.read(addr);
  low = EEPROM.read(addr+1);
  
  val = low + (high << 8);
  return val;
}
  


